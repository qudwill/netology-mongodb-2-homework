'use strict';

const express = require('express');
const bodyParser = require('body-parser');
const mongodb = require('mongodb');
const cors = require('cors');
const MongoClient = mongodb.MongoClient;
const url = 'mongodb://localhost:27017';
const app = express();
const dbName = 'test';
const collectionName = 'contacts';

app.use(cors());
app.use(bodyParser.json());

const restAPI = express.Router();
		
/**
 ** Добавить контакт (имя, фамилия, телефон)
 */

restAPI.post('/contacts/', function(req, res) {
	if (req.body.name && req.body.surname && req.body.phone) {
		MongoClient.connect(url, (err, client) => {
			if (err) {
				throw err;
			} else {
				const db = client.db(dbName);
				const collection = db.collection(collectionName);

				const contacts = [{
					phone: req.body.phone
				}];

				if (req.body.email != '') {
					contacts.push({
						email: req.body.email
					});
				}

				if (req.body.skype != '') {
					contacts.push({
						skype: req.body.skype
					});
				}

				collection.insert({
					name: req.body.name,
					surname: req.body.surname,
					contacts: contacts
				}, (err, result) => {
					(err) ? console.error(err) : res.json(result);
				});

				client.close();
			}
		});
	} else {
		res.status(400);
		res.send();
	}
});


/**
 ** Список добавленных контактов
 */

restAPI.get('/contacts/', function(req, res) {
	MongoClient.connect(url, (err, client) => {
		if (err) {
			throw err;
		} else {
			const db = client.db(dbName);
			const collection = db.collection(collectionName);

			collection.find().toArray((err, result) => {
				if (err) {
					throw err;
				} else if (result.length) {
					res.json(result);
				} else {
					res.status(404);
					res.send();
				}
			});

			client.close();
		}
	});
});

restAPI.get('/contacts/:id', function(req, res) {
	MongoClient.connect(url, (err, client) => {
		if (err) {
			throw err;
		} else {
			const db = client.db(dbName);
			const collection = db.collection(collectionName);

			collection.find({
				_id: new mongodb.ObjectID(req.params.id)
			}).toArray((err, result) => {
				if (err) {
					throw err;
				} else if (result.length) {
					res.json(result);
				} else {
					res.status(404);
					res.send();
				}
			});

			client.close();
		}
	});
});


/**
 ** Удалить контакт
 */

restAPI.delete('/contacts/:id', function(req, res) {
	MongoClient.connect(url, (err, client) => {
		if (err) {
			throw err;
		} else {
			const db = client.db(dbName);
			const collection = db.collection(collectionName);

			collection.deleteOne({
				_id: new mongodb.ObjectID(req.params.id)
			}, (err, result) => {
				(err) ? console.error(err) : res.json(result);
			});

			client.close();
		}
	});
});


/**
 ** Редактировать контакт
 */

restAPI.put('/contacts/:id', function(req, res) {
	if (req.body.name && req.body.surname && req.body.phone) {
		MongoClient.connect(url, (err, client) => {
			if (err) {
				throw err;
			} else {
				const db = client.db(dbName);
				const collection = db.collection(collectionName);

				collection.updateOne({
					_id: new mongodb.ObjectID(req.params.id)
				}, {
					$set: {
						name: req.body.name,
						surname: req.body.surname,
						contacts: [{
							phone: req.body.phone,
							skype: req.body.skype,
							email: req.body.email
						}]
					}
				}, (err, result) => {
					(err) ? console.error(err) : res.json(result);
				});

				client.close();
			}
		});
	} else {
		res.status(404);
		res.send();
	}
});


/**
 ** Поиск по номеру фамилии или имени
 */

restAPI.get('/search/:term', function(req, res) {
	MongoClient.connect(url, (err, client) => {
		if (err) {
			throw err;
		} else {
			const db = client.db(dbName);
			const collection = db.collection(collectionName);

			collection.aggregate([{  
      	$project: {  
      		_id: '$_id',
      		name: '$name',
      		surname: '$surname',
      		contacts: '$contacts',
      		fullName: {  
        		$concat: ['$name', ' ', '$surname']
      		}
      	}
   		}, {  
      	$match: {  
      		fullName: {  
      			$regex: req.params.term
      		}
      	}
   		}]).toArray((err, result) => {
				if (err) {
					throw err;
				} else if (result.length) {
					res.json(result);
				} else {
					res.json('0');
				}
			});

			client.close();
		}
	});
});

app.use('/api/v1', restAPI);
app.listen(3000);