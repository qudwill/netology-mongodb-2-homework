var SERVER_BASE = 'http://localhost:3000';

var App = (function() {
	var $contacts = $('#contacts');

	function setupListeners() {
		$('#add-contact').on('submit', _addContact);
		$(document).on('click', '.remove-contact', _removeContact);
		$(document).on('click', '.edit-contact', _showEditForm);
		$(document).on('submit', '.edit-contact-form', _updateContactInfo);
		$('#search').on('keyup', _searchContacts);
	}

	function _renderContactsList() {
		$.ajax({
			url: SERVER_BASE + '/api/v1/contacts',
			type: 'get',
			success: function(data) {
				if (data) {
					_renderHTML(data);
				} else {
					console.log('Empty data');
				}
			}
		});
	}

	function _addContact(e) {
		e.preventDefault();

		var data = JSON.stringify(_getFormData($(this)));

		$.ajax({
			url: SERVER_BASE + '/api/v1/contacts',
    	contentType: 'application/json',
			type: 'post',
			data: data,
			success: function(data) {
				alert('Контакт ' + data.ops[0].surname + ' ' + data.ops[0].name + ' успешно добавлен');
				_renderContactsList();
			}
		});
	}

	function _getFormData($form) {
    var unindexed_array = $form.serializeArray();
    var indexed_array = {};

    $.map(unindexed_array, function(n, i){
      indexed_array[n['name']] = n['value'];
    });

    return indexed_array;
	}

	function _removeContact(e) {
		e.preventDefault();

		$.ajax({
			url: SERVER_BASE + '/api/v1/contacts/' + $(this).data('id'),
			type: 'delete',
			success: function(data) {
				alert('Удаление пользователя прошло успешно');
				_renderContactsList();
			}
		});
	}

	function _showEditForm(e) {
		e.preventDefault();

		var $this = $(this);
		var _id = $this.data('id');
		var tr = $this.parent().parent();
		var html = '';

		$.ajax({
			url: SERVER_BASE + '/api/v1/contacts/' + _id,
			type: 'get',
			success: function(data) {
				if (data) {
					$.each(data, function(key, item) {
						var contacts = [];

						$.each(item.contacts, function(key, item) {
							$.each(item, function(key, item) {
								contacts[key] = item;
							});
						});

						var skype = (contacts['skype']) ? contacts['skype'] : '';
						var email = (contacts['email']) ? contacts['email'] : '';

						html += '<tr>';
						html += '<td>';
						html += '<form action="" class="edit-contact-form">';
						html += '<input type="hidden" name="_id" value="' + _id + '">';
						html += '<label>Фамилия *</label>';
						html += '<input type="text" name="surname" placeholder="Фамилия" required="" value="' + item.surname + '">';
						html += '<br>';
						html += '<label>Имя *</label>';
						html += '<input type="text" name="name" placeholder="Имя" required="" value="' + item.name + '">';
						html += '<h3>Контактные данные</h3>';
						html += '<label>Телефон *</label>';
						html += '<input type="tel" name="phone" placeholder="Телефон" required="" value="' + contacts['phone'] + '">';
						html += '<br>';
						html += '<label for="skype">Skype</label>';
						html += '<input type="text" name="skype" placeholder="Skype" value="' + skype + '">';
						html += '<br>';
						html += '<label for="email">Email</label>';
						html += '<input type="email" name="email" placeholder="Email" value="' + email + '">';
						html += '<br>';
						html += '<button type="submit">Обновить</button>';
						html += '</form>';
						html += '</td>';
						html += '</tr>';

						tr.after(html);
					});
				} else {
					console.log('Empty data');
				}
			}
		});
	}

	function _updateContactInfo(e) {
		e.preventDefault();

		var formData = _getFormData($(this));
		var data = JSON.stringify(formData);

		$.ajax({
			url: SERVER_BASE + '/api/v1/contacts/' + formData._id,
    	contentType: 'application/json',
			type: 'put',
			data: data,
			success: function(data) {
				alert('Обновление прошло успешно');
				_renderContactsList();
			}
		});
	}

	function _searchContacts() {
		var val = $(this).val();

		console.log(val);

		if (val != '') {
			$.ajax({
				url: SERVER_BASE + '/api/v1/search/' + val,
				type: 'get',
				success: function(data) {
					if (data === '0') {
						$('#contacts').html('Совпадений не найдено');
					} else {
						_renderHTML(data);
					}
				}
			});
		} else {
			_renderContactsList();
		}
	}

	function _renderHTML(data) {
		var html = '<thead><tr><td>Имя</td><td>Контакты</td><td>Редактировать</td><td>Удалить</td></tr></thead>';

		html += '<tbody>';

		$.each(data, function(key, item) {
			html += '<tr>';
			html += '<td>' + item.surname + ' ' + item.name + '</td>';
			html += '<td>';

			if (item.contacts) {
				html += '<ul>';
			}

			$.each(item.contacts, function(key, item) {
				$.each(item, function(key, item) {
					html += '<li>' + key + ': ' + item + '</li>';
				});
			});

			if (item.contacts) {
				html += '</ul>';
			}

			html += '</td>';
			html += '<td>';
			html += '<a href="#" class="edit-contact" data-id="' + item._id + '">Редактировать</a>';
			html += '</td>';
			html += '<td>';
			html += '<a href="#" class="remove-contact" data-id="' + item._id + '">X</a>';
			html += '</td>';
			html += '</tr>';
		});

		html += '</tbody>';

		$contacts.html(html);
	}

	return {
		init: function() {
			setupListeners();
			_renderContactsList();
		}
	}
}());

$(document).ready(function() {
	App.init();
});